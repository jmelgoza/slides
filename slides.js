// slides.js
// https://gist.github.com/2788822
(function($) {
  "use strict";  $(".slide-buttons .next").click(function() {
    var activeimg, firstimg, nextimg, slide;
    slide = $(this).parents().filter(".slides").eq(0);
    activeimg = slide.find(".slide-list li.active");
    nextimg = activeimg.next();
    firstimg = slide.find(".slide-list li:first");
    activeimg.removeClass("active");
    if (nextimg.is("li")) {
      nextimg.addClass("active");
    } else {
      firstimg.addClass("active");
    }
    return false;
  });
  $(".slide-buttons .prev").click(function() {
    var activeimg, lastimg, previmg, slide;
    slide = $(this).parents().filter(".slides").eq(0);
    activeimg = slide.find(".slide-list li.active");
    previmg = activeimg.prev();
    lastimg = slide.find(".slide-list li:last");
    activeimg.removeClass("active");
    if (previmg.is("li")) {
      previmg.addClass("active");
    } else {
      lastimg.addClass("active");
    }
    return false;
  });
  return $(".slide-list > li:first").addClass("active");
})(jQuery);