# slides.js
# https://gist.github.com/2788822
(($) ->
  "use strict"
  $(".slide-buttons .next").click ->
    slide = $(this).parents().filter(".slides").eq(0)
    activeimg = slide.find(".slide-list li.active")
    nextimg = activeimg.next()
    firstimg = slide.find(".slide-list li:first")
    activeimg.removeClass "active"
    if nextimg.is("li")
      nextimg.addClass "active"
    else
      firstimg.addClass "active"
    false

  $(".slide-buttons .prev").click ->
    slide = $(this).parents().filter(".slides").eq(0)
    activeimg = slide.find(".slide-list li.active")
    previmg = activeimg.prev()
    lastimg = slide.find(".slide-list li:last")
    activeimg.removeClass "active"
    if previmg.is("li")
      previmg.addClass "active"
    else
      lastimg.addClass "active"
    false
    
  $(".slide-list > li:first").addClass("active")
  
) jQuery